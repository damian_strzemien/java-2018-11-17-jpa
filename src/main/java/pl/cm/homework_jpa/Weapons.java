package pl.cm.homework_jpa;

import pl.cm.homework_jpa.Goblins;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "weapons")

public class Weapons {

    public enum Damage_type {
        forged,
        secant,
        blunt
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_w;

    @Column
    private String w_name;

    @Column
    private int damage;

    @Enumerated(EnumType.STRING)
    @Column
    private Damage_type damage_type;

    @OneToOne(mappedBy = "weapons")
    private Goblins goblins;

    public Weapons(String w_name, int damage, Enum damage_type) {
        this.w_name = w_name;
        this.damage = damage;
        this.damage_type = (Damage_type) damage_type;
        this.goblins = goblins;
    }


    public int getId_w() {
        return id_w;
    }

    public void setId_w(int id_w) {
        this.id_w = id_w;
    }

    public String getW_name() {
        return w_name;
    }

    public void setW_name(String w_name) {
        this.w_name = w_name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Damage_type getDamage_type() {
        return damage_type;
    }

    public void setDamage_type(Damage_type damage_type) {
        this.damage_type = damage_type;
    }
}
