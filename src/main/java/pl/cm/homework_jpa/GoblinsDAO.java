package pl.cm.homework_jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class GoblinsDAO {

    public Goblins persist(Goblins goblins) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(goblins);
        tx.commit();
        em.close();
        return goblins;
    }

    public Goblins merge(Goblins goblins) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        goblins = em.merge(goblins);
        tx.commit();
        em.close();
        return goblins;
    }

    public Goblins find(int id) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        Goblins goblins = em.find(Caves.class, id);
        em.close();
        return goblins;
    }


    public void delete(Goblins goblins) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(goblins));//Removing must be done in transaction.
        tx.commit();
        em.close();
    }


    public List<Goblins> findAll() {
        EntityManager em = EntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();//Builder for creating a query.
        CriteriaQuery<Goblins> query = cb.createQuery(Goblins.class);//Query object declaring query return object.
        query.from(Goblins.class);//From which entity (table) data will be fetched.
        List<Goblins> goblins = em.createQuery(query).getResultList();
        em.close();
        return goblins;
    }


}
