package pl.cm.homework_jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class WeaponsDAO {

    public Weapons persist(Weapons weapons) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(weapons);
        tx.commit();
        em.close();
        return weapons;
    }

    public Weapons merge(Weapons weapons) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        weapons = em.merge(weapons);
        tx.commit();
        em.close();
        return weapons;
    }

    public Weapons find(int id) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        Weapons weapons = em.find(Weapons.class, id);
        em.close();
        return weapons;
    }


    public void delete(Weapons weapons) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(weapons));//Removing must be done in transaction.
        tx.commit();
        em.close();
    }


    public List<Weapons> findAll() {
        EntityManager em = EntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();//Builder for creating a query.
        CriteriaQuery<Weapons> query = cb.createQuery(Weapons.class);//Query object declaring query return object.
        query.from(Weapons.class);//From which entity (table) data will be fetched.
        List<Weapons> weapons = em.createQuery(query).getResultList();
        em.close();
        return weapons;
    }


}
