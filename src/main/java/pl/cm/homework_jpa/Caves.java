package pl.cm.homework_jpa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "caves")

public class Caves {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_c;

    @Column
    private int area;

    @OneToOne(mappedBy = "caves", cascade = CascadeType.ALL, orphanRemoval = true)
    private Goblins goblins;

    private List<Goblins> goblinsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy="caves")
    public List<Goblins> getGoblinsList() {
        return goblinsList;
    }


//    public Caves(Goblins goblins) {
//        this.goblins = goblins;
//    }

    public Caves(int area) {
        this.area = area;
        this.goblins = goblins;
    }

    public int getId_c() {
        return id_c;
    }

    public void setId_c(int id_c) {
        this.id_c = id_c;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }
}
