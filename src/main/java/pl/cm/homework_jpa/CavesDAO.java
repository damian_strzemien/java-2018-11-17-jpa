package pl.cm.homework_jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CavesDAO {

    public Caves persist(Caves caves) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(caves);
        tx.commit();
        em.close();
        return caves;
    }

    public Caves merge(Caves caves) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        caves = em.merge(caves);
        tx.commit();
        em.close();
        return caves;
    }

    public Caves find(int id) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        Caves caves = em.find(Caves.class, id);
        em.close();
        return caves;
    }


    public void delete(Caves caves) {
        EntityManager em = EntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(caves));//Removing must be done in transaction.
        tx.commit();
        em.close();
    }


    public List<Caves> findAll() {
        EntityManager em = EntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();//Builder for creating a query.
        CriteriaQuery<Caves> query = cb.createQuery(Caves.class);//Query object declaring query return object.
        query.from(Caves.class);//From which entity (table) data will be fetched.
        List<Caves> caves = em.createQuery(query).getResultList();
        em.close();
        return caves;
    }


}
