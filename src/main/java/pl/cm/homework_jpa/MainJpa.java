package pl.cm.homework_jpa;

import javax.persistence.EntityManagerFactory;

import static pl.cm.homework_jpa.Weapons.Damage_type.blunt;
import static pl.cm.homework_jpa.Weapons.Damage_type.forged;
import static pl.cm.homework_jpa.Weapons.Damage_type.secant;

public class MainJpa {

    public static void main(String[] args) {
        CavesDAO cavesDAO = new CavesDAO();
        GoblinsDAO goblinsDAO = new GoblinsDAO();
        WeaponsDAO weaponsDAO = new WeaponsDAO();

        Caves sweetHome1 = new Caves(100);
        Caves sweetHome2 = new Caves(150);
        Caves sweetHome3 = new Caves(50);

        Goblins Lucek = new Goblins("Lucek", 12, forged);
        Goblins Pacek = new Goblins("Pacek", 12, secant);
        Goblins Mycek = new Goblins("Mycek", 12, blunt);
        Goblins Ziutek = new Goblins("Ziutek", 12, blunt);
        Goblins Edek = new Goblins("Edek", 12, secant);
        Goblins Ferdek = new Goblins("Ferdek", 12, secant);
        Goblins Tadek = new Goblins("Tadek", 12, forged);
        Goblins Tadzio = new Goblins("Tadzio", 12, forged);

        Weapons weapon1 = new Weapons("Axe",25,secant);
        Weapons weapon2 = new Weapons("Hammer",24,blunt);


        cavesDAO.persist(sweetHome1);
        cavesDAO.persist(sweetHome2);
        cavesDAO.persist(sweetHome3);

        goblinsDAO.persist(Lucek);
        goblinsDAO.persist(Pacek);
        goblinsDAO.persist(Mycek);
        goblinsDAO.persist(Ziutek);
        goblinsDAO.persist(Edek);
        goblinsDAO.persist(Ferdek);
        goblinsDAO.persist(Tadek);
        goblinsDAO.persist(Tadzio);

        weaponsDAO.persist(weapon1);
        weaponsDAO.persist(weapon2);

        cavesDAO.findAll().forEach(System.out::println);
        goblinsDAO.findAll().forEach(System.out::println);
        weaponsDAO.findAll().forEach(System.out::println);



        System.out.println("---");

//        EntityManagerFactory.close();
    }

}
