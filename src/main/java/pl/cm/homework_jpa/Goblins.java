package pl.cm.homework_jpa;

import pl.cm.homework_jpa.Caves;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "goblins")

public class Goblins {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_g;

    @Column
    private String name;

    @Column
    private int height;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="w_name", unique = true)
    private Enum weapons;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="id_c", unique = true)

    private Caves caves;

    @ManyToOne(optional = true)
    @JoinColumn(name="id_c")
    public Caves getCaves() {
        return caves;
    }


    public Goblins(String name, int height, Enum weapons) {
        this.name = name;
        this.height = height;
        this.weapons = weapons;
        this.caves = caves;
    }

    public int getId_g() {
        return id_g;
    }

    public void setId_g(int id_g) {
        this.id_g = id_g;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
